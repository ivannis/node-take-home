import { Message } from '@core/cqrs/message';

export abstract class Command<
  TPayload extends object,
> extends Message<TPayload> {
  constructor(payload: TPayload) {
    super(payload, {});
  }
}
