import { Message } from '@core/cqrs/message';

export abstract class Event<TPayload extends object> extends Message<TPayload> {
  protected constructor(payload: TPayload) {
    super(payload, {});
  }
}
