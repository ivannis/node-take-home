import { AggregateRoot } from '@core/aggregate/aggregate-root';
import { EntitySchema, UuidType } from '@mikro-orm/core';

export const AggregateRootSchema = new EntitySchema<AggregateRoot>({
  name: 'AggregateRoot',
  abstract: true,
  properties: {
    id: { type: UuidType, primary: true },
    createdAt: { type: Date },
    updatedAt: { type: Date },
  },
});
