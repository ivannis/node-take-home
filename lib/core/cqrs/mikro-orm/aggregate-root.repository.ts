import { AggregateRoot } from '@core/aggregate/aggregate-root';
import { DomainEvent } from '@core/aggregate/domain-event';
import { Repository } from '@core/aggregate/repository';
import {
  EntityRepository,
  FilterQuery,
  EntityManager,
  EntityName,
} from '@mikro-orm/core';
import { NotFoundException } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';

export class AggregateRootRepository<TAggregateRoot extends AggregateRoot>
  implements Repository<TAggregateRoot>
{
  private readonly repo: EntityRepository<TAggregateRoot>;

  constructor(
    model: EntityName<TAggregateRoot>,
    entityManager: EntityManager,
    private readonly eventEmitter: EventEmitter2,
  ) {
    this.repo = entityManager.getRepository<TAggregateRoot>(model);
  }

  async get(id: TAggregateRoot['id']): Promise<TAggregateRoot> {
    const aggregate = await this.findOne({ id } as FilterQuery<TAggregateRoot>);
    if (aggregate === null) {
      throw new NotFoundException(`Entity not found: ${id}`);
    }

    return aggregate;
  }

  async persist(aggregate: TAggregateRoot): Promise<void> {
    const events = aggregate.commit();
    await this.repo.upsert(aggregate);

    events.forEach((event: DomainEvent) => {
      this.eventEmitter.emit(event.constructor.name, event);
    });
  }

  async delete(id: TAggregateRoot['id']): Promise<void> {
    this.repo.removeAndFlush(await this.get(id));
  }

  async findOne(
    entityFilterQuery?: FilterQuery<TAggregateRoot>,
  ): Promise<TAggregateRoot | null> {
    return this.repo.findOne(entityFilterQuery, {});
  }

  async find(
    entityFilterQuery?: FilterQuery<TAggregateRoot>,
  ): Promise<TAggregateRoot[]> {
    return this.repo.find(entityFilterQuery, {});
  }

  async findAll(): Promise<TAggregateRoot[]> {
    return this.repo.findAll({});
  }
}
