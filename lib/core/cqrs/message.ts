export type Metadata = Record<string, unknown>;

export abstract class Message<
  TPayload extends object,
  TMetadata extends Metadata = Metadata,
> {
  protected constructor(
    public readonly payload: TPayload,
    public metadata: TMetadata,
  ) {}
}
