import { ulid as baseUlid } from 'ulid';

export type Id = string;

export const ulid = (seedTime?: number): Id => {
  return baseUlid(seedTime).toLocaleLowerCase();
};

export const genId = (type: string, seedTime?: number): Id => {
  return `${type}_${baseUlid(seedTime).toLocaleLowerCase()}`;
};

export const isId = (type: string, id: Id) => {
  const [actualType, actualId] = id.split('_', 2);

  return type === actualType && actualId !== '';
};
