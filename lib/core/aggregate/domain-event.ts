import { Id } from '@core/aggregate/id';
import { Event } from '@core/cqrs/event';

export type Payload<TId = Id> = {
  id: TId;
};

export abstract class DomainEvent<
  TId = Id,
  TPayload extends Payload<TId> = Payload<TId>,
> extends Event<TPayload> {
  constructor(payload: TPayload) {
    super(payload);
  }
}
