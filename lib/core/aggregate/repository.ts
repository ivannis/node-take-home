import { AggregateRoot } from '@core/aggregate/aggregate-root';

export interface Repository<T extends AggregateRoot> {
  get(id: T['id']): Promise<T>;
  persist(aggregate: T): Promise<void>;
}
