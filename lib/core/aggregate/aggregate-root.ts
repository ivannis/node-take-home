import { DomainEvent } from '@core/aggregate/domain-event';
import { Id } from '@core/aggregate/id';

const INTERNAL_EVENTS = Symbol();

export abstract class AggregateRoot<TId = Id> {
  private [INTERNAL_EVENTS]: DomainEvent<TId>[];

  protected createdAt: Date;
  protected updatedAt: Date;
  protected constructor(readonly id: TId) {}

  commit() {
    const events = [...this[INTERNAL_EVENTS]];
    this[INTERNAL_EVENTS].length = 0;

    return events;
  }

  uncommit() {
    this[INTERNAL_EVENTS].length = 0;
  }

  protected record<T extends DomainEvent<TId>>(event: T) {
    if (!this[INTERNAL_EVENTS]) {
      this[INTERNAL_EVENTS] = [];
    }

    this[INTERNAL_EVENTS].push(event);

    this.updatedAt = new Date();
    if (!this.createdAt) {
      this.createdAt = this.updatedAt;
    }
  }
}
