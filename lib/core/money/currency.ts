import { CurrencyCode } from '@core/money/currency-code';
import { EUR, USD, GBP } from '@dinero.js/currencies';

const currencies = {
  EUR,
  USD,
  GBP,
};

export type Currency = {
  readonly code: CurrencyCode;
  readonly base: number;
  readonly exponent: number;
};

export const getCurrency = (code: CurrencyCode): Currency => {
  if (!currencies[code]) {
    throw new Error(`Unsupported currency ${code}`);
  }

  return currencies[code] as Currency;
};
