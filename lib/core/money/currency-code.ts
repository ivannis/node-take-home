export enum CurrencyCodeEnum {
  EUR = 'EUR',
  USD = 'USD',
  GBP = 'GBP',
  INR = 'INR',
}

export type CurrencyCode = keyof typeof CurrencyCodeEnum;
