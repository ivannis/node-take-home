import { getCurrency } from '@core/money/currency';
import { CurrencyCode } from '@core/money/currency-code';
import { toDecimal, dinero } from 'dinero.js';

export type Money = {
  amount: number;
  currency: CurrencyCode;
};

export const toDinero = ({ amount, currency }: Money) => {
  return dinero({ amount, currency: getCurrency(currency) });
};

export const intlFormat = (
  money: Money,
  locale?: string,
  options?: Intl.NumberFormatOptions,
): string => {
  return toDecimal(toDinero(money), ({ value, currency }) =>
    Number(value).toLocaleString(locale, {
      ...options,
      style: 'currency',
      currency: currency.code,
    }),
  );
};

export const money = (amount: number, currency: CurrencyCode): Money => ({
  amount,
  currency,
});

export const zero = (currency: CurrencyCode): Money => ({
  amount: 0,
  currency,
});
