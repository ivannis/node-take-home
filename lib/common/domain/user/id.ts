import { AssertionError } from 'assert';

import { genId, Id, isId } from '@core/aggregate';

export type UserId = Id;
export const genUserId = (seedTime?: number) => genId('usr', seedTime);

export function isUserId(id: Id): id is UserId {
  return isId('usr', id);
}

export function assertIsUserId(id: Id): asserts id is UserId {
  if (!isUserId(id)) {
    throw new AssertionError({
      message: `Invalid user id: "${id}"`,
      actual: id,
    });
  }
}
