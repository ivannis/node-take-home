import { Hasher } from '@common/domain/shared';
import { assertIsUserId, UserId, Role, RoleEnum } from '@common/domain/user';
import {
  SignedInEvent,
  UserCreatedEvent,
  UserPromotedEvent,
} from '@common/domain/user/events';
import { InvalidCredentialsException } from '@common/domain/user/exceptions';
import { AggregateRoot } from '@core/aggregate';

export class User extends AggregateRoot<UserId> {
  private username: string;
  private password: string;
  private role: Role;

  constructor(id: UserId) {
    super(id);
  }

  static async create(
    id: UserId,
    username: string,
    password: string,
    hasher: Hasher,
  ): Promise<User> {
    assertIsUserId(id);

    const user = new User(id);
    user.username = username;
    user.password = await hasher.hash(password);
    user.role = RoleEnum.USER;

    user.record(
      new UserCreatedEvent({
        id: user.id,
        username: user.username,
        password: user.password,
        role: user.role,
      }),
    );

    return user;
  }

  async singIn(password: string, hasher: Hasher): Promise<void> {
    if (!this.password) {
      throw new InvalidCredentialsException();
    }

    if (!(await hasher.check(password, this.password))) {
      throw new InvalidCredentialsException();
    }

    this.record(new SignedInEvent({ id: this.id }));
  }

  promote(role: Role): void {
    // TODO: We can add some constraints to prevent assigning a lower role to the user.
    // But that only makes sense if we have some kind of role hierarchy.

    this.role = role;

    this.record(new UserPromotedEvent({ id: this.id, role: this.role }));
  }

  public getRole() {
    return this.role;
  }
}
