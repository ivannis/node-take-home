export enum RoleEnum {
  USER = 'USER',
  ADMIN = 'ADMIN',
}

export type Role = keyof typeof RoleEnum;
