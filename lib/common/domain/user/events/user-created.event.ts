import { Role, UserId } from '@common/domain/user';
import { DomainEvent } from '@core/aggregate';

export class UserCreatedEvent extends DomainEvent<
  UserId,
  { id: UserId; username: string; password: string; role: Role }
> {}
