import { UserId } from '@common/domain/user';
import { DomainEvent } from '@core/aggregate';

export class SignedInEvent extends DomainEvent<
  UserId,
  {
    id: UserId;
  }
> {}
