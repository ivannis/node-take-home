import { Role, UserId } from '@common/domain/user';
import { DomainEvent } from '@core/aggregate';

export class UserPromotedEvent extends DomainEvent<
  UserId,
  { id: UserId; role: Role }
> {}
