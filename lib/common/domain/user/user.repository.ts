import { User, UserId } from '@common/domain/user';
import { Repository } from '@core/aggregate';

export abstract class UserRepository implements Repository<User> {
  abstract get(id: UserId): Promise<User>;

  abstract persist(aggregate: User): Promise<void>;

  abstract getByUsername(username: string): Promise<User | null>;

  abstract findByIds(ids: UserId[]): Promise<User[]>;
}
