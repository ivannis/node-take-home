export abstract class Hasher {
  abstract hash(value: string, saltOrRounds?: string | number): Promise<string>;
  abstract check(value: string, hashedValue: string): Promise<boolean>;
}
