import { UserId, Role } from '@common/domain/user';

export type Credential = {
  id: UserId;
  role: Role;
};
