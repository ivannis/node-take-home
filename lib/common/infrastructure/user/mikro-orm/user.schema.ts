import { User } from '@common/domain/user';
import { AggregateRoot } from '@core/aggregate';
import { EntitySchema } from '@mikro-orm/core';

export const UserSchema = new EntitySchema<User, AggregateRoot>({
  name: 'user',
  class: User,
  properties: {
    username: { type: 'string', unique: true },
    password: { type: 'string' },
    role: { type: 'string' },
  },
});
