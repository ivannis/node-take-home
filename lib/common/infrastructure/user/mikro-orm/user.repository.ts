import { UserId, User, UserRepository } from '@common/domain/user';
import { AggregateRootRepository } from '@core/cqrs';
import { FilterQuery, EntityManager } from '@mikro-orm/core';
import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';

@Injectable()
export class InMemoryUserRepository extends UserRepository {
  private readonly repo: AggregateRootRepository<User>;

  constructor(entityManager: EntityManager, eventEmitter: EventEmitter2) {
    super();

    this.repo = new AggregateRootRepository<User>(
      User,
      entityManager,
      eventEmitter,
    );
  }

  get(id: UserId): Promise<User> {
    return this.repo.get(id);
  }

  persist(aggregate: User): Promise<void> {
    return this.repo.persist(aggregate);
  }

  getByUsername(username: string): Promise<User | null> {
    return this.repo.findOne({ username } as FilterQuery<User>);
  }

  findByIds(ids: UserId[]): Promise<User[]> {
    return this.repo.find({
      id: { $in: ids },
    } as FilterQuery<User>);
  }
}
