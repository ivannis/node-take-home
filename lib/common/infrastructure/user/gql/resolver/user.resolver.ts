import { AuthService } from '@common/application/auth';
import { UserService } from '@common/application/user';
import { Credential } from '@common/domain/auth';
import {
  AuthenticatedUser,
  GqlAuthGuard,
  LocalAuthGuard,
  IsPublic,
} from '@common/infrastructure/auth';
import { AuthResolver, GqlContext } from '@common/infrastructure/shared';
import { SingInInput } from '@common/infrastructure/user/gql/input';
import { Token } from '@common/infrastructure/user/gql/token';
import { User } from '@common/infrastructure/user/gql/user';
import { UserLoader } from '@common/infrastructure/user/gql/user.loader';
import { UnauthorizedException, UseGuards } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Args, Context, Mutation, Query, Resolver } from '@nestjs/graphql';

@Resolver((_of) => User)
export class UserResolver extends AuthResolver {
  constructor(
    private readonly users: UserService,
    private readonly auth: AuthService,
    private readonly userLoader: UserLoader,
    config: ConfigService,
  ) {
    super(config);
  }

  @UseGuards(GqlAuthGuard)
  @Query((_returns) => User, { name: 'me' })
  async me(@AuthenticatedUser() @AuthenticatedUser() { id }: Credential) {
    const user = await this.userLoader.load(id);

    if (user) {
      return user;
    }

    throw new UnauthorizedException();
  }

  @IsPublic()
  @UseGuards(LocalAuthGuard)
  @Mutation(() => Token, { name: 'singIn' })
  async singIn(
    @Args('input') input: SingInInput,
    @AuthenticatedUser() { id }: Credential,
    @Context() context: GqlContext,
  ) {
    const user = await this.users.get(id);
    const token = this.auth.singIn(user);

    this.responseWithCookie(token, context);

    return { token };
  }
}
