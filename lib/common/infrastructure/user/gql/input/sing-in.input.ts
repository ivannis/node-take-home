import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class SingInInput {
  @Field()
  username: string;

  @Field()
  password: string;
}
