import { UserService } from '@common/application/user';
import { UserId, User } from '@common/domain/user';
import { DataLoader } from '@common/infrastructure/shared';
import { Injectable, Scope } from '@nestjs/common';

@Injectable({ scope: Scope.REQUEST })
export class UserLoader extends DataLoader<UserId, User> {
  constructor(users: UserService) {
    super((ids: UserId[]) => users.findByIds(ids));
  }
}
