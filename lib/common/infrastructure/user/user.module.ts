import { UserService } from '@common/application/user';
import { UserRepository } from '@common/domain/user';
import {
  InMemoryUserRepository,
  UserSchema,
  UserResolver,
  UserLoader,
} from '@common/infrastructure/user';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { Module } from '@nestjs/common';

const eventHandlers = [];
const resolvers = [UserResolver, UserLoader];
const persistence = [InMemoryUserRepository];

@Module({
  imports: [MikroOrmModule.forFeature([UserSchema])],
  controllers: [],
  providers: [
    {
      provide: UserRepository,
      useClass: InMemoryUserRepository,
    },
    ...eventHandlers,
    ...resolvers,
    ...persistence,
    UserService,
  ],
  exports: [UserService, UserLoader],
})
export class UserModule {}
