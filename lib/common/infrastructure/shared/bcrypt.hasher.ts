import { Hasher } from '@common/domain/shared';
import { compare, hash } from 'bcrypt';

export class BcryptHasher extends Hasher {
  private readonly rounds: number = 10;

  hash(value: string, saltOrRounds: string | number): Promise<string> {
    return hash(value, saltOrRounds ?? this.rounds);
  }

  check(value: string, hashedValue: string): Promise<boolean> {
    return compare(value, hashedValue);
  }
}
