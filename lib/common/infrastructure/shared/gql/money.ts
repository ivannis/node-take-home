import { CurrencyCode } from '@common/infrastructure/shared';
import { Money as MoneyType } from '@core/money';
import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Money {
  @Field(() => Int)
  amount: number;

  @Field(() => CurrencyCode)
  currency: CurrencyCode;

  @Field(() => String)
  formatted?: string;
}

export const fromMoney = (money: MoneyType): Money => ({
  amount: money.amount,
  currency: CurrencyCode[money.currency],
});
