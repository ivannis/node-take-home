import { CurrencyCodeEnum as CurrencyCode } from '@core/money';
import { registerEnumType } from '@nestjs/graphql';

registerEnumType(CurrencyCode, {
  name: 'CurrencyCode',
});

export default CurrencyCode;
