import { Id } from '@core/aggregate';
import * as DataLoaderBase from 'dataloader';

export abstract class DataLoader<
  TId extends Id = Id,
  T extends { id: TId } = { id: TId },
> implements DataLoaderBase<TId, T>
{
  private readonly loader: DataLoaderBase<TId, T, TId>;

  protected constructor(
    batchLoadFn: DataLoaderBase.BatchLoadFn<TId, T>,
    options?: DataLoaderBase.Options<TId, T, TId>,
  ) {
    this.loader = new DataLoaderBase(async (ids) => {
      const result = await batchLoadFn(ids);
      const map = new Map((result as Array<T>).map((item) => [item.id, item]));

      return ids.map((id) => map.get(id));
    }, options);
  }
  clear(key: TId): this {
    this.loader.clear(key);

    return this;
  }

  clearAll(): this {
    this.loader.clearAll();

    return this;
  }

  load(key: TId): Promise<T> {
    return this.loader.load(key);
  }

  loadMany(keys: ArrayLike<TId>): Promise<Array<Error | T>> {
    return this.loader.loadMany(keys);
  }

  prime(key: TId, value: Error | T): this {
    this.loader.prime(key, value);

    return this;
  }
}
