import { Money } from '@common/infrastructure/shared';
import { intlFormat } from '@core/money';
import { Args, Parent, ResolveField, Resolver } from '@nestjs/graphql';

@Resolver((_of) => Money)
export class MoneyResolver {
  @ResolveField(() => String, { name: 'formatted' })
  formatted(
    @Parent() money: Money,
    @Args('locale', { nullable: true }) locale?: string,
  ) {
    return intlFormat(money, locale);
  }
}
