import { GqlContext } from '@common/infrastructure/shared';
import { ConfigService } from '@nestjs/config';
import { addMilliseconds } from 'date-fns';
import * as ms from 'ms';

export abstract class AuthResolver {
  protected constructor(protected readonly config: ConfigService) {}

  protected responseWithCookie(token: string, { res: response }: GqlContext) {
    const milliseconds = ms(this.config.get<string>('JWT_TTL') ?? '7d');
    const domain = this.config.get<string>('APP_DOMAIN');

    response.cookie('jwt', token, {
      expires: addMilliseconds(new Date(), milliseconds),
      httpOnly: true,
      path: '/',
      domain,
    });
  }
}
