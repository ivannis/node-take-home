import { Hasher } from '@common/domain/shared';
import { BcryptHasher, MoneyResolver } from '@common/infrastructure/shared';
import { Global, Module } from '@nestjs/common';

@Global()
@Module({
  imports: [],
  providers: [
    {
      provide: Hasher,
      useClass: BcryptHasher,
    },
    MoneyResolver,
  ],
  exports: [Hasher],
})
export class SharedModule {}
