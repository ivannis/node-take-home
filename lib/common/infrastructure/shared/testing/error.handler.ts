export const parseErrors = (error: Error): any[] => {
  const { response } = JSON.parse(JSON.stringify(error, undefined, 2)) as any;

  return response.errors;
};
