import { ModuleMetadata } from '@nestjs/common';
import { Test } from '@nestjs/testing';

import { SessionFactory } from './session.builder';

export const createTestingApp = async (modules: ModuleMetadata['imports']) => {
  const module = await Test.createTestingModule({
    imports: [...modules],
  }).compile();

  const testingApp = module.createNestApplication();
  const sessionFactory = new SessionFactory(testingApp);
  await testingApp.init();

  return { sessionFactory, testingApp };
};
