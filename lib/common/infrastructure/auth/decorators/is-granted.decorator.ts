import { Role } from '@common/domain/user';
import { SetMetadata } from '@nestjs/common';

export const IsGranted = (role: Role) => SetMetadata('role', role);
