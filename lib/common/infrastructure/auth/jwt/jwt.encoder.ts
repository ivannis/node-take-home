import { Credential } from '@common/domain/auth';
import { UserId } from '@common/domain/user';
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

export type TokenPayload = Omit<Credential, 'id'> & { sub: UserId };

@Injectable()
export class JwtEncoder {
  constructor(private jwtService: JwtService) {}

  sign(user: Credential): string {
    const payload: TokenPayload = {
      ...user,
      sub: user.id,
    };

    return this.jwtService.sign(payload);
  }
}
