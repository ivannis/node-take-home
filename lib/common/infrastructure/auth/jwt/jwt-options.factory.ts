import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import type {
  JwtModuleOptions,
  JwtOptionsFactory as JwtOptionsFactoryInterface,
} from '@nestjs/jwt';

@Injectable()
export class JwtOptionsFactory implements JwtOptionsFactoryInterface {
  constructor(private readonly config: ConfigService) {}

  createJwtOptions(): JwtModuleOptions {
    const secret = this.config.get<string>('JWT_SECRET_KEY');
    const expiresIn =
      this.config.get<string | number | undefined>('JWT_TTL') ?? '7d';

    return {
      secret,
      signOptions: {
        expiresIn,
      },
    };
  }
}
