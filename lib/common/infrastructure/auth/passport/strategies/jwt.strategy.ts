import { Credential } from '@common/domain/auth';
import { TokenPayload } from '@common/infrastructure/auth';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import type { Request } from 'express';
import { ExtractJwt, Strategy } from 'passport-jwt';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly config: ConfigService) {
    super({
      jwtFromRequest: (request: Request) => {
        if (request && request.cookies && request.cookies.jwt) {
          return request.cookies['jwt'];
        }

        return ExtractJwt.fromAuthHeaderAsBearerToken()(request);
      },
      ignoreExpiration: false,
      secretOrKey: config.get<string>('JWT_SECRET_KEY'),
    });
  }

  async validate(payload: TokenPayload): Promise<Credential> {
    return Promise.resolve({ ...payload, id: payload.sub });
  }
}
