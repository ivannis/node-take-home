import {
  ExecutionContext,
  ContextType as CommonContextType,
} from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AuthGuard } from '@nestjs/passport';

type ContextType = CommonContextType | 'graphql';

export class LocalAuthGuard extends AuthGuard('local') {
  getRequest(context: ExecutionContext) {
    if (context.getType<ContextType>() === 'graphql') {
      const ctx = GqlExecutionContext.create(context);

      const request = ctx.getContext().req;
      request.body = ctx.getArgs().input;

      return request;
    }

    return context;
  }
}
