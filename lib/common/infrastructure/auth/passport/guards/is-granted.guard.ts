import { Role } from '@common/domain';
import {
  Injectable,
  ExecutionContext,
  CanActivate,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { GqlExecutionContext } from '@nestjs/graphql';

@Injectable()
export class IsGrantedhGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const ctx = GqlExecutionContext.create(context);
    const role = this.reflector.get<Role>('role', ctx.getHandler());

    if (!role) {
      return true;
    }

    const request = ctx.getContext().req;
    const user = request.user || undefined;

    if (user && user.role !== role) {
      throw new UnauthorizedException();
    }

    return true;
  }
}
