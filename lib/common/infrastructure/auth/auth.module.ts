import { AuthService } from '@common/application/auth';
import {
  JwtOptionsFactory,
  JwtEncoder,
  JwtStrategy,
  LocalStrategy,
  IsGrantedhGuard,
  GqlAuthGuard,
} from '@common/infrastructure/auth';
import { UserModule } from '@common/infrastructure/user';
import { Global, Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

const strategies = [LocalStrategy, JwtStrategy];

@Global()
@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({
      useClass: JwtOptionsFactory,
    }),
    UserModule,
  ],
  providers: [
    ...strategies,
    JwtEncoder,
    AuthService,
    {
      provide: APP_GUARD,
      useClass: IsGrantedhGuard,
    },
    {
      provide: APP_GUARD,
      useClass: GqlAuthGuard,
    },
  ],
  exports: [AuthService],
})
export class AuthModule {}
