import { UserService } from '@common/application/user';
import { Credential } from '@common/domain/auth';
import { User } from '@common/domain/user';
import { JwtEncoder } from '@common/infrastructure/auth';
import { Injectable, UnauthorizedException } from '@nestjs/common';

@Injectable()
export class AuthService {
  constructor(
    private readonly users: UserService,
    private readonly jwtEncoder: JwtEncoder,
  ) {}

  async authenticate(username: string, password: string): Promise<Credential> {
    const user = await this.users.getByUsername(username);
    if (!user) {
      throw new UnauthorizedException();
    }

    await this.users.singIn({ id: user.id, password });

    return AuthService.createCredential(user);
  }

  singIn(user: User): string {
    return this.jwtEncoder.sign(AuthService.createCredential(user));
  }

  private static createCredential(user: User): Credential {
    return { id: user.id, role: user.getRole() };
  }
}
