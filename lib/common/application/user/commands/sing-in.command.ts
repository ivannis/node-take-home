import { UserId } from '@common/domain/user';
import { Command } from '@core/cqrs/command';

export class SingInCommand extends Command<{ id: UserId; password: string }> {}
