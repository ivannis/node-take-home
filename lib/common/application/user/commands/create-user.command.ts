import { UserId } from '@common/domain/user';
import { Command } from '@core/cqrs/command';

export class CreateUserCommand extends Command<{
  id: UserId;
  username: string;
  password: string;
}> {}
