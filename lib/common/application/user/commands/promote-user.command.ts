import { Role, UserId } from '@common/domain/user';
import { Command } from '@core/cqrs/command';

export class PromoteUserCommand extends Command<{
  id: UserId;
  role: Role;
}> {}
