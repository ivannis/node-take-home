import {
  CreateUserCommand,
  SingInCommand,
  PromoteUserCommand,
} from '@common/application/user';
import { Hasher } from '@common/domain/shared';
import { UserId, User, UserRepository } from '@common/domain/user';
import { Injectable } from '@nestjs/common';

@Injectable()
export class UserService {
  constructor(
    private readonly users: UserRepository,
    private readonly hasher: Hasher,
  ) {}
  get(id: UserId) {
    return this.users.get(id);
  }

  getByUsername(username: string) {
    return this.users.getByUsername(username);
  }

  findByIds(ids: UserId[]) {
    return this.users.findByIds(ids);
  }

  async create({ id, username, password }: CreateUserCommand['payload']) {
    const user = await User.create(id, username, password, this.hasher);

    await this.users.persist(user);
  }

  async singIn({ id, password }: SingInCommand['payload']) {
    const user = await this.users.get(id);
    await user.singIn(password, this.hasher);

    await this.users.persist(user);
  }

  async promote({ id, role }: PromoteUserCommand['payload']) {
    const user = await this.users.get(id);
    user.promote(role);

    await this.users.persist(user);
  }
}
