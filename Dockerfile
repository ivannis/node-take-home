# Base image
FROM node:18-alpine

# Create app directory
WORKDIR /app

COPY package.json yarn.lock ./

# Install app dependencies
RUN yarn install --frozen-lockfile

# Bundle app source
COPY . .

# Creates a "dist" folder with the production build
RUN yarn build

CMD ["yarn", "start:prod"]