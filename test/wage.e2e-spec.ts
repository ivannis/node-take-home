import { createTestingApp, parseErrors } from '@common/infrastructure';
import { INestApplication } from '@nestjs/common';
import { UserSeeder } from 'database/seeders/UserSeeder';
import { WageSeeder } from 'database/seeders/WageSeeder';
import { GraphQLClient } from 'graphql-request';

import { AppModule } from '@app/app.module';
import { WageService } from '@app/domain';

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import { Sdk, getSdk, CurrencyCode, SingInInput } from './gql/queries'; // this file is generated at runtime
import { seedDatabase } from './helpers';

process.env.MIKRO_ORM_ALLOW_GLOBAL_CONTEXT = '1';
process.env.MIKRO_ORM_ALLOW_GLOBAL_CLI = '1';
process.env.MIKRO_ORM_ALLOW_VERSION_MISMATCH = '1';

describe('wages (e2e)', () => {
  let app: INestApplication;
  let graphQLClient: GraphQLClient;
  let session: Sdk;

  const signIn = async (
    input: SingInInput = {
      username: 'john',
      password: 'guess',
    },
  ) => {
    const { singIn } = await session.singIn({ input });

    graphQLClient.setHeader('Authorization', `Bearer ${singIn.token}`);
  };

  beforeEach(async () => {
    const { sessionFactory, testingApp } = await createTestingApp([AppModule]);

    graphQLClient = await sessionFactory.create();
    session = getSdk(graphQLClient);
    app = testingApp;
  });

  it('should create a new wage', async () => {
    const service = app.get<WageService>(WageService);
    await seedDatabase(app, UserSeeder);
    await signIn();

    const { createWage } = await session.createWage({
      input: {
        name: 'Willy',
        salary: 180000,
        currency: CurrencyCode.Usd,
        department: 'it',
        sub_department: 'internal',
      },
    });

    expect((await service.findAll()).length).toEqual(1);
    expect(createWage).toHaveProperty('id');
    expect(createWage).toEqual(
      expect.objectContaining({
        name: 'Willy',
        salary: 180000,
        currency: CurrencyCode.Usd,
        department: 'it',
        sub_department: 'internal',
        on_contract: false,
      }),
    );
  });

  it('should delete a wage', async () => {
    const service = app.get<WageService>(WageService);
    await seedDatabase(app, UserSeeder, WageSeeder);
    await signIn();

    expect((await service.findAll()).length).toEqual(9);

    const { deleteWage } = await session.deleteWage({
      id: 'wag_01gqpxqpr8xm9304cv4hprjn06',
    });

    expect((await service.findAll()).length).toEqual(8);

    expect(deleteWage).toHaveProperty('id');
    expect(deleteWage).toEqual(
      expect.objectContaining({
        name: 'Abhishek',
        salary: 145000,
        currency: CurrencyCode.Usd,
        department: 'Engineering',
        sub_department: 'Platform',
      }),
    );
  });

  it('delete a wage should throw an exception if the wage doesn"t exists', async () => {
    await seedDatabase(app, UserSeeder);
    await signIn();

    try {
      await session.deleteWage({
        id: 'wag_01gqprtwch5wxfannp_invalid',
      });
    } catch (error) {
      const errors = parseErrors(error);

      expect(errors.length).toEqual(1);
      expect(errors[0].extensions.response.statusCode).toEqual(404);
    }
  });

  it('should calculate stats properly', async () => {
    await seedDatabase(app, UserSeeder, WageSeeder);
    await signIn();

    const { stats: allStats } = await session.stats();
    expect(allStats).toEqual({
      min: 30,
      max: 200000000,
      mean: 22295010,
    });

    const { stats: noContractStats } = await session.stats({
      on_contract: false,
    });
    expect(noContractStats).toEqual({
      min: 30,
      max: 200000000,
      mean: 28636441,
    });

    const { stats: contractStats } = await session.stats({
      on_contract: true,
    });
    expect(contractStats).toEqual({
      min: 90000,
      max: 110000,
      mean: 100000,
    });
  });

  it('should return empty stats if there is no wage records', async () => {
    await seedDatabase(app, UserSeeder);
    await signIn();

    const { stats } = await session.stats();
    expect(stats).toEqual({
      min: null,
      max: null,
      mean: null,
    });
  });

  it('should calculate stats by department properly', async () => {
    await seedDatabase(app, UserSeeder, WageSeeder);
    await signIn();

    const { statsByDepartment } = await session.statsByDepartment();

    expect(statsByDepartment.length).toEqual(4);
    expect(statsByDepartment).toEqual([
      {
        min: 30,
        max: 30,
        mean: 30,
        department: 'Administration',
      },
      {
        min: 90000,
        max: 90000,
        mean: 90000,
        department: 'Banking',
      },
      {
        min: 30,
        max: 200000000,
        mean: 40099006,
        department: 'Engineering',
      },
      {
        min: 30,
        max: 70000,
        mean: 35015,
        department: 'Operations',
      },
    ]);
  });

  it('should return empty stats by department if there is no wage records', async () => {
    await seedDatabase(app, UserSeeder);
    await signIn();

    const { statsByDepartment } = await session.statsByDepartment();

    expect(statsByDepartment.length).toEqual(0);
    expect(statsByDepartment).toEqual([]);
  });

  it('should calculate stats by sub_department properly', async () => {
    await seedDatabase(app, UserSeeder, WageSeeder);
    await signIn();

    const { statsBySubdepartment } = await session.statsBySubdepartment();

    expect(statsBySubdepartment.length).toEqual(4);
    expect(statsBySubdepartment).toEqual([
      {
        min: 30,
        max: 30,
        mean: 30,
        department: 'Administration',
        sub_department: 'Agriculture',
      },
      {
        min: 90000,
        max: 90000,
        mean: 90000,
        department: 'Banking',
        sub_department: 'Loan',
      },
      {
        min: 30,
        max: 200000000,
        mean: 40099006,
        department: 'Engineering',
        sub_department: 'Platform',
      },
      {
        min: 30,
        max: 70000,
        mean: 35015,
        department: 'Operations',
        sub_department: 'CustomerOnboarding',
      },
    ]);
  });

  it('should return empty stats by sub_department if there is no wage records', async () => {
    await seedDatabase(app, UserSeeder);
    await signIn();

    const { statsBySubdepartment } = await session.statsBySubdepartment();

    expect(statsBySubdepartment.length).toEqual(0);
    expect(statsBySubdepartment).toEqual([]);
  });
});
