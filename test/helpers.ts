import { MikroORM, Constructor } from '@mikro-orm/core';
import { Seeder } from '@mikro-orm/seeder';
import { INestApplication } from '@nestjs/common';

export const seedDatabase = async (
  app: INestApplication,
  ...classNames: Constructor<Seeder>[]
): Promise<void> => {
  // Get seeder from MikroORM
  const orm = app.get<MikroORM>(MikroORM);
  const seeder = orm.getSeeder();

  // Refresh the database to start clean
  await orm.getSchemaGenerator().refreshDatabase();

  // Seed the testing database
  await seeder.seed(...classNames);
};
