import { createTestingApp, parseErrors } from '@common/infrastructure';
import { INestApplication } from '@nestjs/common';
import { UserSeeder } from 'database/seeders/UserSeeder';
import { GraphQLClient } from 'graphql-request';

import { AppModule } from '@app/app.module';

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import { Sdk, getSdk } from './gql/queries'; // this file is generated at runtime
import { seedDatabase } from './helpers';

describe('users (e2e)', () => {
  let app: INestApplication;
  let graphQLClient: GraphQLClient;
  let session: Sdk;

  beforeEach(async () => {
    const { sessionFactory, testingApp } = await createTestingApp([AppModule]);

    graphQLClient = await sessionFactory.create();
    session = getSdk(graphQLClient);
    app = testingApp;
  });

  it('should sign-in a user', async () => {
    await seedDatabase(app, UserSeeder);

    const { singIn } = await session.singIn({
      input: {
        username: 'john',
        password: 'guess',
      },
    });

    expect(singIn).toHaveProperty('token');
  });

  it('sign-in should throw an exception if the user doesn"t exists', async () => {
    try {
      await session.singIn({
        input: {
          username: 'user',
          password: 'password',
        },
      });
    } catch (error) {
      const errors = parseErrors(error);

      expect(errors.length).toEqual(1);
      expect(errors[0].extensions.response.statusCode).toEqual(401);
    }
  });

  it('sign-in should throw an exception if the credential doesn"t match', async () => {
    await seedDatabase(app, UserSeeder);

    try {
      await session.singIn({
        input: {
          username: 'john',
          password: 'bar',
        },
      });
    } catch (error) {
      const errors = parseErrors(error);

      expect(errors.length).toEqual(1);
      expect(errors[0].message).toEqual('Unauthorized');
    }
  });

  it('should get user profile', async () => {
    await seedDatabase(app, UserSeeder);

    const { singIn } = await session.singIn({
      input: {
        username: 'john',
        password: 'guess',
      },
    });

    graphQLClient.setHeader('Authorization', `Bearer ${singIn.token}`);

    const { me } = await session.me();
    expect(me.username).toEqual('john');
    expect(me.role).toEqual('USER');
  });

  it('get user profile should throw an exception if JWT is not provided', async () => {
    await seedDatabase(app, UserSeeder);

    try {
      await session.me();
    } catch (error) {
      const errors = parseErrors(error);

      expect(errors.length).toEqual(1);
      expect(errors[0].message).toEqual('Unauthorized');
    }
  });
});
