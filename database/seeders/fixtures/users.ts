import { genUserId } from '@common/domain';

export const users = [
  {
    id: genUserId(),
    username: 'john',
    password: 'guess',
  },
  {
    id: genUserId(),
    username: 'maria',
    password: 'changeme',
  },
  {
    id: genUserId(),
    username: 'admin',
    password: 'admin',
  },
];
