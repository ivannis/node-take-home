import { Seeder } from '@mikro-orm/seeder';
import { EntityManager } from '@mikro-orm/sqlite';
import { EventEmitter2 } from '@nestjs/event-emitter';

import { WageProjector, WageService } from '@app/domain';
import { InMemoryWageRepository } from '@app/infrastructure';

import { wages } from './fixtures/wages';

export class WageSeeder extends Seeder {
  async run(em: EntityManager): Promise<void> {
    const emitter = new EventEmitter2();
    const service = new WageService(new InMemoryWageRepository(em, emitter));
    new WageProjector(emitter);

    await Promise.all(
      wages.map(async (wage) => {
        await service.create(wage);
      }),
    );
  }
}
