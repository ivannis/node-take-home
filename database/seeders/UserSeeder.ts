import { UserService } from '@common/application/user/user.service';
import { RoleEnum } from '@common/domain/user';
import { BcryptHasher } from '@common/infrastructure/shared';
import { InMemoryUserRepository } from '@common/infrastructure/user';
import type { EntityManager } from '@mikro-orm/core';
import { Seeder } from '@mikro-orm/seeder';
import { EventEmitter2 } from '@nestjs/event-emitter';

import { users } from './fixtures/users';

export class UserSeeder extends Seeder {
  async run(em: EntityManager): Promise<void> {
    const emitter = new EventEmitter2();

    const service = new UserService(
      new InMemoryUserRepository(em, emitter),
      new BcryptHasher(),
    );

    await Promise.all(
      users.map(async (user) => {
        await service.create(user);
      }),
    );

    const adminId = users[users.length - 1].id;
    await service.promote({ id: adminId, role: RoleEnum.ADMIN });
  }
}
