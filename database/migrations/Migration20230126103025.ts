import { Migration } from '@mikro-orm/migrations';

export class Migration20230126103025 extends Migration {
  async up(): Promise<void> {
    this.addSql(
      'alter table `wage` add column `on_contract` integer not null;',
    );
  }
}
