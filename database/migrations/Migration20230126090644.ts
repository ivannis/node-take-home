import { Migration } from '@mikro-orm/migrations';

export class Migration20230126090644 extends Migration {
  async up(): Promise<void> {
    this.addSql(
      'create table `wage` (`id` text not null, `created_at` datetime not null, `updated_at` datetime not null, `name` text not null, `salary` integer not null, `currency` text not null, `department` text not null, `sub_department` text not null, primary key (`id`));',
    );
  }
}
