import { join } from 'path';

import { AuthModule } from '@common/infrastructure/auth';
import { SharedModule } from '@common/infrastructure/shared';
import { UserModule } from '@common/infrastructure/user';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { GraphQLModule } from '@nestjs/graphql';

import { WageModule } from '@app/infrastructure';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    EventEmitterModule.forRoot(),
    MikroOrmModule.forRoot(),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: join(process.cwd(), 'schema.gql'),
      context: ({ req, res }) => ({ req, res }),
      debug: process.env.NODE_ENV !== 'production',
      playground: process.env.NODE_ENV !== 'production',
      cache: process.env.NODE_ENV !== 'production' ? undefined : 'bounded',
      subscriptions: {
        'graphql-ws': true,
      },
    }),
    SharedModule,
    AuthModule,
    UserModule,
    WageModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
