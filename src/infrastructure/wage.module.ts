import { MikroOrmModule } from '@mikro-orm/nestjs';
import { Module } from '@nestjs/common';

import { WageRepository, WageService, WageProjector } from '@app/domain';
import {
  InMemoryWageRepository,
  WageSchema,
  WageResolver,
  WageLoader,
} from '@app/infrastructure';

const eventHandlers = [];
const resolvers = [WageResolver, WageLoader];
const persistence = [InMemoryWageRepository];

@Module({
  imports: [MikroOrmModule.forFeature([WageSchema])],
  controllers: [],
  providers: [
    {
      provide: WageRepository,
      useClass: InMemoryWageRepository,
    },
    ...eventHandlers,
    ...resolvers,
    ...persistence,
    WageService,
    WageProjector,
  ],
  exports: [WageService, WageLoader],
})
export class WageModule {}
