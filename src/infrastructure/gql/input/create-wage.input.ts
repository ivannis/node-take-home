import CurrencyCode from '@common/infrastructure/shared/gql/currency-code';
import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class CreateWageInput {
  @Field()
  name: string;

  @Field()
  salary: number;

  @Field(() => CurrencyCode)
  currency: CurrencyCode;

  @Field()
  department: string;

  @Field()
  sub_department: string;

  @Field({ nullable: true })
  on_contract?: boolean;
}
