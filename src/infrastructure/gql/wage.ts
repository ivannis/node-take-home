import CurrencyCode from '@common/infrastructure/shared/gql/currency-code';
import { Field, GraphQLISODateTime, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Wage {
  @Field(() => ID)
  id: string;

  @Field(() => String)
  name: string;

  @Field(() => Number)
  salary: number;

  @Field(() => CurrencyCode)
  currency: CurrencyCode;

  @Field(() => String)
  department: string;

  @Field(() => String)
  sub_department: string;

  @Field(() => Boolean)
  on_contract: boolean;

  @Field(() => GraphQLISODateTime)
  createdAt: Date;

  @Field(() => GraphQLISODateTime)
  updatedAt: Date;
}
