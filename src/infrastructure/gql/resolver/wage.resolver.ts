import { GqlAuthGuard } from '@common/infrastructure/auth';
import { UseGuards } from '@nestjs/common';
import { Args, Query, Mutation, Resolver, ID } from '@nestjs/graphql';

import { genWageId, WageService } from '@app/domain';
import { CreateWageInput } from '@app/infrastructure/gql/input';
import {
  Stats,
  StatsByDepartment,
  StatsBySubdepartment,
} from '@app/infrastructure/gql/stats';
import { Wage } from '@app/infrastructure/gql/wage';

@Resolver((_of) => Wage)
export class WageResolver {
  constructor(private readonly wages: WageService) {}

  @UseGuards(GqlAuthGuard)
  @Mutation((_returns) => Wage, { name: 'createWage' })
  async createWage(
    @Args('input')
    {
      name,
      salary,
      currency,
      department,
      sub_department,
      on_contract,
    }: CreateWageInput,
  ) {
    const id = genWageId();

    await this.wages.create({
      id,
      name,
      salary,
      currency,
      department,
      sub_department,
      on_contract,
    });

    return await this.wages.get(id);
  }

  @UseGuards(GqlAuthGuard)
  @Mutation((_returns) => Wage, { name: 'deleteWage' })
  async deleteWage(@Args('id', { type: () => ID }) id: string) {
    return this.wages.delete({ id });
  }

  @UseGuards(GqlAuthGuard)
  @Query((_returns) => Stats, { name: 'stats' })
  async stats(
    @Args('on_contract', { type: () => Boolean, nullable: true })
    onContract?: boolean,
  ) {
    return this.wages.findStats(onContract);
  }

  @UseGuards(GqlAuthGuard)
  @Query((_returns) => [StatsByDepartment], { name: 'statsByDepartment' })
  async statsByDepartment() {
    return this.wages.findStatsByDepartment();
  }

  @UseGuards(GqlAuthGuard)
  @Query((_returns) => [StatsBySubdepartment], { name: 'statsBySubdepartment' })
  async statsBySubdepartment() {
    return this.wages.findStatsBySubdepartment();
  }
}
