import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Stats {
  @Field(() => Number, { nullable: true })
  min: number;

  @Field(() => Number, { nullable: true })
  max: number;

  @Field(() => Number, { nullable: true })
  mean: number;
}

@ObjectType()
export class StatsByDepartment extends Stats {
  @Field(() => String, { nullable: true })
  department: number;
}

@ObjectType()
export class StatsBySubdepartment extends StatsByDepartment {
  @Field(() => String, { nullable: true })
  sub_department: number;
}
