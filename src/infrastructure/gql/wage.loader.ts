import { DataLoader } from '@common/infrastructure/shared';
import { Injectable, Scope } from '@nestjs/common';

import { WageService, WageId, Wage } from '@app/domain';

@Injectable({ scope: Scope.REQUEST })
export class WageLoader extends DataLoader<WageId, Wage> {
  constructor(wages: WageService) {
    super((ids: WageId[]) => wages.findByIds(ids));
  }
}
