import { AggregateRootRepository } from '@core/cqrs';
import { FilterQuery } from '@mikro-orm/core';
import { EntityManager } from '@mikro-orm/sqlite';
import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';

import {
  WageId,
  Wage,
  WageRepository,
  StatsProjection,
  StatsByDepartmentProjection,
  StatsBySubdepartmentProjection,
} from '@app/domain';

@Injectable()
export class InMemoryWageRepository extends WageRepository {
  private readonly repo: AggregateRootRepository<Wage>;

  constructor(
    private readonly entityManager: EntityManager,
    eventEmitter: EventEmitter2,
  ) {
    super();

    this.repo = new AggregateRootRepository<Wage>(
      Wage,
      entityManager,
      eventEmitter,
    );
  }

  get(id: WageId): Promise<Wage> {
    return this.repo.get(id);
  }

  persist(aggregate: Wage): Promise<void> {
    return this.repo.persist(aggregate);
  }

  delete(id: WageId): Promise<void> {
    return this.repo.delete(id);
  }

  findStats(onContract?: boolean): Promise<StatsProjection> {
    return this.entityManager
      .createQueryBuilder(Wage)
      .select(
        'min(salary) as min, max(salary) as max, ROUND(avg(salary), 0) as mean',
      )
      .where(onContract !== undefined ? { on_contract: onContract } : {})
      .execute('get');
  }

  findStatsByDepartment(): Promise<StatsByDepartmentProjection[]> {
    return this.entityManager
      .createQueryBuilder(Wage)
      .select(
        'department, min(salary) as min, max(salary) as max, ROUND(avg(salary), 0) as mean',
      )
      .groupBy('department')
      .execute('all');
  }

  findStatsBySubdepartment(): Promise<StatsBySubdepartmentProjection[]> {
    return this.entityManager
      .createQueryBuilder(Wage)
      .select(
        'department, sub_department, min(salary) as min, max(salary) as max, ROUND(avg(salary), 0) as mean',
      )
      .groupBy(['department', 'sub_department'])
      .execute('all');
  }

  findAll(): Promise<Wage[]> {
    return this.repo.findAll();
  }

  findByIds(ids: WageId[]): Promise<Wage[]> {
    return this.repo.find({
      id: { $in: ids },
    } as FilterQuery<Wage>);
  }
}
