import { AggregateRoot } from '@core/aggregate';
import { EntitySchema } from '@mikro-orm/core';

import { Wage } from '@app/domain';

export const WageSchema = new EntitySchema<Wage, AggregateRoot>({
  name: 'wage',
  class: Wage,
  properties: {
    name: { type: 'string' },
    salary: { type: 'number' },
    currency: { type: 'string' },
    department: { type: 'string' },
    sub_department: { type: 'string' },
    on_contract: { type: 'boolean' },
  },
});
