import { NestFactory } from '@nestjs/core';
import * as cookieParser from 'cookie-parser';

import { AppModule } from '@app/app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app
    // cookie and cors
    .use(cookieParser())
    .enableCors({
      credentials: true,
      allowedHeaders: [
        'X-API-KEY',
        'Origin',
        'Authorization',
        'X-Requested-With',
        'User-Language',
        'Content-Type',
        'Accept',
        'Access-Control-Request-Method',
      ],
      methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS', 'HEAD'],
      origin: true,
    });

  await app.listen(3000);
}

bootstrap().catch(console.error);
