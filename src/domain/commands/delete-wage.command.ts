import { Command } from '@core/cqrs/command';

import { WageId } from '@app/domain';

export class DeleteWageCommand extends Command<{
  id: WageId;
}> {}
