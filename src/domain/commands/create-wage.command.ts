import { Command } from '@core/cqrs/command';
import { CurrencyCode } from '@core/money';

import { WageId } from '@app/domain';

export class CreateWageCommand extends Command<{
  id: WageId;
  name: string;
  salary: number;
  currency: CurrencyCode;
  department: string;
  sub_department: string;
  on_contract?: boolean;
}> {}
