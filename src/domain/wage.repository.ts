import { Repository } from '@core/aggregate';

import { Wage, WageId } from '@app/domain';
import {
  StatsProjection,
  StatsByDepartmentProjection,
  StatsBySubdepartmentProjection,
} from '@app/domain/projections';

export abstract class WageRepository implements Repository<Wage> {
  abstract get(id: WageId): Promise<Wage>;

  abstract persist(aggregate: Wage): Promise<void>;

  abstract delete(id: WageId): Promise<void>;

  abstract findStats(onContract?: boolean): Promise<StatsProjection>;

  abstract findStatsByDepartment(): Promise<StatsByDepartmentProjection[]>;

  abstract findStatsBySubdepartment(): Promise<
    StatsBySubdepartmentProjection[]
  >;

  abstract findAll(): Promise<Wage[]>;

  abstract findByIds(ids: WageId[]): Promise<Wage[]>;
}
