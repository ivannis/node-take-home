import { Injectable } from '@nestjs/common';

import {
  CreateWageCommand,
  DeleteWageCommand,
  WageId,
  Wage,
  WageRepository,
} from '@app/domain';

@Injectable()
export class WageService {
  constructor(private readonly wages: WageRepository) {}
  get(id: WageId) {
    return this.wages.get(id);
  }

  findAll() {
    return this.wages.findAll();
  }

  findByIds(ids: WageId[]) {
    return this.wages.findByIds(ids);
  }

  findStats(onContract?: boolean) {
    return this.wages.findStats(onContract);
  }

  findStatsByDepartment() {
    return this.wages.findStatsByDepartment();
  }

  findStatsBySubdepartment() {
    return this.wages.findStatsBySubdepartment();
  }

  async create({
    id,
    name,
    salary,
    currency,
    department,
    sub_department,
    on_contract = false,
  }: CreateWageCommand['payload']) {
    const wage = Wage.create(
      id,
      name,
      salary,
      currency,
      department,
      sub_department,
      on_contract,
    );

    await this.wages.persist(wage);
  }

  async delete({ id }: DeleteWageCommand['payload']) {
    const wage = await this.get(id);
    wage.delete();

    await this.wages.delete(id);

    return wage;
  }
}
