import { DomainEvent } from '@core/aggregate';

import { WageId } from '@app/domain';

export class WageDeletedEvent extends DomainEvent<
  WageId,
  {
    id: WageId;
  }
> {}
