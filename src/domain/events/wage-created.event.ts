import { DomainEvent } from '@core/aggregate';
import { CurrencyCode } from '@core/money';

import { WageId } from '@app/domain';

export class WageCreatedEvent extends DomainEvent<
  WageId,
  {
    id: WageId;
    name: string;
    salary: number;
    currency: CurrencyCode;
    department: string;
    sub_department: string;
    on_contract: boolean;
  }
> {}
