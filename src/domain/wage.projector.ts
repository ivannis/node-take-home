import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';

import { WageCreatedEvent, WageDeletedEvent } from '@app/domain/events';

/**
 * A projection based on the events stream is a very powerful technique that can be used to generate the stats
 * in realtime. If the dataset grows exponentially and the system begins to have performance problems,
 * an event-based projection can prepare the data to be read instantly.
 *
 * Any nosql database can be used to persist the projections. For example, if we were to use mongo-db,
 * this projector would keep these three collections updated every time a wage is added or removed.
 *
 * collection: stats (only one document with the global stats)
 * {
 *    "min": 0,
 *    "max": 0,
 *    "mean": 0,
 * }
 *
 * collection: stats_by_department (one document with the stats per department)
 * {
 *    "min": 0,
 *    "max": 0,
 *    "mean": 0,
 *    "department": "Administration",
 * }
 * {
 *    "min": 0,
 *    "max": 0,
 *    "mean": 0,
 *    "department": "Banking",
 * }
 * ....
 * collection: stats_by_sub_department (one document with the stats per department, sub_department conbination)
 * {
 *    "min": 0,
 *    "max": 0,
 *    "mean": 0,
 *    "department": "Administration",
 *    "sub_department": "Agriculture",
 * }
 * {
 *    "min": 0,
 *    "max": 0,
 *    "mean": 0
 *    "department": "Banking",
 *    "sub_department": "Loan",
 * }
 * ....
 **/
@Injectable()
export class WageProjector {
  constructor(private readonly eventEmitter: EventEmitter2) {
    this.eventEmitter.on(WageCreatedEvent.name, this.handleWageCreatedEvent);
    this.eventEmitter.on(WageDeletedEvent.name, this.handleWageDeletedEvent);
  }

  handleWageCreatedEvent({
    payload: { salary, department, sub_department },
  }: WageCreatedEvent) {
    // TODO: The following pseudocode is only indicative to give an idea of how it could be done,
    // it is not a real implementation.
    //
    // if (!recordInStats) {
    //   this.createStatsRecord();
    // } else {
    //   this.updateStatsRecord(salary);
    // }
    //
    // if (!recordInStatsByDepartment(department)) {
    //   this.createStatsByDepartmentRecord();
    // } else {
    //   this.updateStatsByDepartmentRecord(salary, department);
    // }
    //
    // if (!recordInStatsBySubdepartment(department, sub_department)) {
    //   this.createStatsBySubDepartmentRecord();
    // } else {
    //   this.updateStatsBySubDepartmentRecord(salary, department, sub_department);
    // }
  }

  handleWageDeletedEvent(event: WageDeletedEvent) {
    // TODO: update all the stats
  }

  createStatsRecord() {
    // TODO: create a record in stats
  }

  createStatsByDepartmentRecord() {
    // TODO: create a record in stats_by_department
  }

  createStatsBySubDepartmentRecord() {
    // TODO: create a record in stats_by_sub_department
  }

  updateStatsRecord(salary: number) {
    // TODO: update the stats record
  }

  updateStatsByDepartmentRecord(salary: number, department: string) {
    // TODO: update the department record in stats_by_department
  }

  updateStatsBySubDepartmentRecord(
    salary: number,
    department: string,
    sub_department: string,
  ) {
    // TODO: update the department, sub_department record in stats_by_sub_department
  }
}
