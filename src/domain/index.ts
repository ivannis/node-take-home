export * from './commands';
export * from './events';
export * from './projections';
export * from './id';
export * from './wage.projector';
export * from './wage.repository';
export * from './wage.service';
export * from './wage';
