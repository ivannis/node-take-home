import { StatsByDepartmentProjection } from '@app/domain/projections';

export type StatsBySubdepartmentProjection = StatsByDepartmentProjection & {
  sub_department: string;
};
