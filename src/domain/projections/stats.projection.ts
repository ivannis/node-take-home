export type StatsProjection = {
  min: number;
  max: number;
  mean: number;
};
