import { StatsProjection } from '@app/domain/projections';

export type StatsByDepartmentProjection = StatsProjection & {
  department: string;
};
