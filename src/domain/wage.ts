import { AggregateRoot } from '@core/aggregate';
import { CurrencyCode } from '@core/money';

import { assertIsWageId, WageId } from '@app/domain';
import { WageCreatedEvent, WageDeletedEvent } from '@app/domain/events';

export class Wage extends AggregateRoot<WageId> {
  private name: string;
  private salary: number;
  private currency: CurrencyCode;
  private department: string;
  private sub_department: string;
  private on_contract: boolean;

  constructor(id: WageId) {
    super(id);
  }

  static create(
    id: WageId,
    name: string,
    salary: number,
    currency: CurrencyCode,
    department: string,
    sub_department: string,
    on_contract: boolean,
  ): Wage {
    assertIsWageId(id);

    const wage = new Wage(id);
    wage.name = name;
    wage.salary = salary;
    wage.currency = currency;
    wage.department = department;
    wage.sub_department = sub_department;
    wage.on_contract = on_contract;

    wage.record(
      new WageCreatedEvent({
        id: wage.id,
        name: wage.name,
        salary: wage.salary,
        currency: wage.currency,
        department: wage.department,
        sub_department: wage.sub_department,
        on_contract: wage.on_contract,
      }),
    );

    return wage;
  }

  delete(): void {
    this.record(new WageDeletedEvent({ id: this.id }));
  }
}
