import { AssertionError } from 'assert';

import { genId, Id, isId } from '@core/aggregate';

export type WageId = Id;
export const genWageId = (seedTime?: number) => genId('wag', seedTime);

export function isWageId(id: Id): id is WageId {
  return isId('wag', id);
}

export function assertIsWageId(id: Id): asserts id is WageId {
  if (!isWageId(id)) {
    throw new AssertionError({
      message: `Invalid wage id: "${id}"`,
      actual: id,
    });
  }
}
