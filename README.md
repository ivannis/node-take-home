## Introduction

Wages is a micro-services application to keep track of market wages. A NodeJS application using NestJS 8.0., Graphql and SQLite for persistence.

## Getting Started

You must install the following dependencies before starting:

- [Git](https://git-scm.com/)
- [Docker](https://docs.docker.com/compose/install/)

## Installation

```bash
# copy and edit the env file
$ cp .env.dist .env
$ yarn install
# setup the database with initial user data
$ yarn mikro-orm migration:fresh
$ yarn mikro-orm seeder:run
```

## Running the app

```bash
# development
$ docker-compose up

# production mode
$ docker-compose -f production.yml up
```

This will start the web server on localhost, port 3000: http://localhost:3000/graphql

If you are running the application in development mode you will then see the GraphQL playground, as shown below.

![playground](playground.png)

Or you can download one of the following graphQL clients:

- [Altair](https://altair.sirmuel.design)
- [Graphql playground](https://github.com/prisma-labs/graphql-playground)
- [GraphiQL](https://github.com/graphql/graphiql)

## Test

```bash
# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```

## Usage

At this point, the application is up and running and exposes a graphQL API that can be accessed at the url http://localhost:3000/graphql. The API is protected and can only be accessed with the correct credentials.

When initializing your application the seed script populated the database with some [users](database/seeders/fixtures/users.ts) data.

### Authentication

This API uses Bearer authentication to authenticate the users.

To get access to the API, first make sure you have some of the credentials of the [users](database/seeders/fixtures/users.ts) created during the seeding process.

To do so, call the `singIn` mutation. It will return a unique access token.

```graphql
mutation {
  singIn(input: { username: "john", password: "guess" }) {
    token
  }
}
```

The access token that comes in the response must be sent in each of the requests that you are going to make to the API.

In order to do so, you must create a new `Authorization` header with value `Bearer {access token}`.

The token is valid for a limited time. When an access token has expired, the API call will return the HTTP status code 401 Unauthorized and a new access token has to be acquired.

### User profile

The following query will fetch the logged user information.

```graphql
query {
  me {
    id
    username
    role
    createdAt
    updatedAt
  }
}
```

### Create new wage record

To create a new wage record use the `createWage` mutation. Replace {{YOUR_...}} with your custom values.

```graphql
mutation {
  createWage(
    input: {
      name: "{{YOUR_NAME}}",
      salary: {{YOUR_SALARY}}
      currency: {{YOUR_CURRENCY}}
      department: {{YOUR_DEPARTMENT}}
      sub_department: {{YOUR_SUB_DEPARTMENT}}
      on_contract: {{true|false|undefined}}
    }
  ) {
    id
    name
    salary
    currency
    department
    sub_department
    on_contract,
    createdAt
    updatedAt
  }
}
```

### Delete a wage record

If you want to delete a wage record, use `deleteWage` mutation.

```graphql
mutation {
  deleteWage(id: "{{YOUR_WAGE_ID}}") {
    id
    name
    salary
    currency
    department
    sub_department
    createdAt
    updatedAt
  }
}
```

### Get statistics

The following query will fetch the summary statistics of the entire dataset.

You can optional populate the database with some fixtures with the following command:

```bash
$ yarn mikro-orm seeder:run --class=WageSeeder
```

```graphql
query {
  stats(on_contract: {{true|false|undefined}}) {
    min
    max
    mean
  }
}
```

### Get statistics by department

The query below lets you access the summary statistics for each department.

```graphql
query {
  statsByDepartment {
    min
    max
    mean
  }
}
```

### Get statistics by sub department

You can also use the `statsBySubdepartment` query to get statistic for each department and sub-department combination.

```graphql
query {
  statsBySubdepartment {
    min
    max
    mean
  }
}
```

## Architecture

This project implement a simplified event-driven architecture with two layers that makes it easy to change and upgrade the application during and after the development process.

### Domain layer

Contains the code that touches and implements business logic. Domain classes, repositories, services, commands and events should be declared in this layer.

### Infrastructure layer

Contains anything that the application needs to work (database persistence, graphql interface, framework-related staff, etc).

The folder structure for the application looks as follows:

```bash
├── database             // contains the database seeds and migration files
├── lib                  // contains the core lib that support the event-driven architecture
├── src
│   ├── domain
│   │   ├── commands      // contains all the domain commands (intent to do something)
│   │   ├── events        // contains all the domain events (fact that happened)
│   │   ├── repository.ts // define the persistence layer
│   │   ├── service.ts    // expose the domain api to the infrastructure layer (the entry point)
│   │   ├── entity.ts     // contains all the business logic
│   │   └── id.ts
│   ├── infrastructure
│   │   ├── gql           // contains the graphql ui (inputs, resolvers, types, etc)
│   │   └── mikro-orm     // contains the database layer (schemas, repositories)
│   ├── ...
├── test
├── ...
```

## Ensure code quality

Strategies that I use to ensure code safety and quality:

- Automated testing
- Code coverage
- Continuous integration and deployment
- Code reviews
- Static code analysis
- Proper Documentation
- Code simplification
- Proper Error handling and logging
- Performance optimization

## Code examples

Let me illustrate a basic example that helps you understand this architecture clearly and know how to implement it effectively.

Let's say we wanted to implement a new functionality to update the currency for one specific record that already exists.

### Step 1: Commands and events

Starting with the easiest part, let's creates the command and events:

```typescript
# src/domain/commands/update-wage-currency.command.ts
import { Command } from '@core/cqrs/command';
import { CurrencyCode } from '@core/money';

import { WageId } from '@app/domain';

export class UpdateWageCurrencyCommand extends Command<{
  id: WageId;
  currency: CurrencyCode;
}> {}
```

```typescript
# src/domain/events/wage-currency-updated.event.ts
import { DomainEvent } from '@core/aggregate';
import { CurrencyCode } from '@core/money';

import { WageId } from '@app/domain';

export class WageCurrencyUpdatedEvent extends DomainEvent<
  WageId,
  {
    id: WageId;
    currency: CurrencyCode;
  }
> {}
```

### Step 2: Update the entity

Now is time to implement our business logic:

```typescript
# src/domain/wage.ts
import { CurrencyCode } from '@core/money';
import { WageCurrencyUpdatedEvent } from '@app/domain/events';


export class Wage extends AggregateRoot<WageId> {
  ...

  updateCurrency(currency: CurrencyCode): void {
    this.currency = currency;

    this.record(new WageCurrencyUpdatedEvent({ id: this.id, currency }));
  }
}
```

### Step 3: Update the service

And the last step is to make this functionality available outside of the domain.

```typescript
# src/domain/wage.service.ts
import { UpdateWageCurrencyCommand } from '@app/domain/command';


export class WageService {
  ...

  async updateCurrency({ id, currency }: UpdateWageCurrencyCommand['payload']) {
    const wage = await this.get(id);
    wage.updateCurrency(currency);

    await this.wages.persist(wage);
  }
}
```

At this point, you can decide if you want to add a new mutation to the graphql API to expose this functionality.

## Stay in touch

- Author - [Ivan Suarez](https://github.com/ivannis)
