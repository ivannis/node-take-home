import { UserSchema } from '@common/infrastructure/user';
import { AggregateRootSchema } from '@core/cqrs/mikro-orm';
import { MikroORMOptions } from '@mikro-orm/core';

import { WageSchema } from '@app/infrastructure';

const config: Partial<MikroORMOptions> = {
  entities: [AggregateRootSchema, UserSchema, WageSchema],
  dbName: 'my-db.sqlite3',
  type: 'sqlite',
  seeder: {
    path: './database/seeders',
    pathTs: undefined,
    defaultSeeder: 'DatabaseSeeder',
    glob: '!(*.d).{js,ts}',
    emit: 'ts',
    fileName: (className: string) => className,
  },
  migrations: {
    path: './database/migrations',
    pathTs: undefined,
  },
};

export default config;
